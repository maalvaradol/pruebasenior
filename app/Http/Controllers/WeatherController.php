<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WeatherController extends Controller
{
    function buildBaseString($baseURI, $methods, $params){
        $r = [];
        ksort($params);
        foreach ($params as $key => $value) {
            $r[] = "$key=". rawurlencode($value);
        }

        return $methods."&".rawurlencode($baseURI)."&".rawurlencode(implode("&", $r));
    }

    function buildAuthorizacionHeader($oauth){
        $r = 'Authorization: OAuth ';
        $values = array();
        foreach ($oauth as $key => $value) {
            $values[] = "$key=\"". rawurlencode($value). "\"";
        }
        
        $r .= implode(',',$values);

        return $r;
    }

    function buildResponse($city,$region){

        $url = "https://weather-ydn-yql.media.yahoo.com/forecastrss";
        $app_id = "XubHLoDb";
        $consumer_key = "dj0yJmk9YkVqc0t4aGEwUHFZJmQ9WVdrOVdIVmlTRXh2UkdJbWNHbzlNQT09JnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTE2";
        $consumer_secret = "45d1ff69b84857d15d833c0175d0b2c2c463594e";        

        $query = array(
            'location' =>  $city.",".$region,
            'format' => 'json'
        );
    
        $oauth = array(
            'oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => uniqid(mt_rand(1,1000)),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0',
        );
    
        $base_info = $this->buildBaseString($url, 'GET', array_merge($query,$oauth));
        $composite_key = rawurlencode($consumer_secret)."&";
        $oauth_signature = base64_encode(hash_hmac('sha1',$base_info,$composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;
    
        $header = array(
            $this->buildAuthorizacionHeader($oauth),
            'X-Yahoo-App-Id: '. $app_id 
        );
    
        $options = array(
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $url."?".http_build_query($query),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        ); 
        
        $ch = curl_init();
        curl_setopt_array($ch, $options);

        //dd($options);

        $response = curl_exec($ch);
        curl_close($ch);

        //print_r($response);
        return $response;
        
    }

}
