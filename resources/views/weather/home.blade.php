<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>APP - Clima</title>

    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0W2OcdV5gvqTfrwj9G3k0169ugDxul0o&callback=initMap&libraries=&v=weekly" defer></script>

    <style>
        .card{
            border-radius: 10px;
        }
        .card img{
            border-radius: 10px;
        }
        #map {
            height: 80%;
        }        
    </style>
</head>
<body>

    <div class="container mt-3 mb-3">
        <div class="row">

            <div class="col-lg-12">
                <a type="button" class="btn btn-info" href="historial">VER HISTORIAL</a>
            </div>

            <div class="col-lg-8" id="div_cities">
            </div>

            <div class="col-lg-4 col-sm-12 col-xs-12" style="height: 600px;border-radius:10px;">
                <h4 class="mt-2">Maps</h4>
                <div id="map"></div>
            </div>

        </div>
    </div>


    <script>

    let div_cities = document.getElementById('div_cities'); 
    cargarTemperatura();

    let lat = 4.65637;
    let lng = -74.11779;

    function initMap() {
        const myLatLng = { lat: lat, lng: lng};
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 4,
            center: myLatLng,
        });
        new google.maps.Marker({
            position: myLatLng,
            map,
            title: "Maps",
        });
    }

    function cargarTemperatura(){
       
        url = "/appSenior/public/api/weather";

        const cities = [
            {'ciudad':'orlando','region':'fl','img':'https://miamidiario.com/wp-content/uploads/Orlando.jpg'},
            {'ciudad':'miami','region':'fl','img':'https://images.prismic.io/vivaair/58716930644f19713c91bd3da8d6a15c505d24ca_ciudad-tiquetes-baratos-a-miami.jpg?auto=compress,format'},
            {'ciudad':'new york','region':'ny','img':'https://joyvacationsclub.com/wp-content/uploads/2019/07/NY.jpg'}
        ];

        for (const ciudad of cities) {
            fetch(url+"/"+ciudad.ciudad+"/"+ciudad.region)
            .then((resp) => resp.json())
            .catch((error) => console.log(error))
            .then((response) => {
                renderizarTarjeta(response,ciudad)
                guardarHistorial(response)
            });          
        }
    }

    
    function renderizarTarjeta(datos,ciudad){
            
        div_cities.innerHTML += tarjeta = `<div class="col-lg-12 mt-2">
            <div class="card text-white">
                <img src="${ciudad.img}" height="280">
                <div class="card-img-overlay">
                <h2 class="card-text">${datos.location.country}</h2>
                <h3 class="card-title">${datos.location.ciudad} ${datos.location.region}</h3>
                <p class="card-text">${datos.location.timezone_id}</p>
                <h1>Temperatura ${datos.current_observation.condition.temperature} °</h1>
                <button type="button" class="btn btn-warning" onclick="seleccionarMapa(${datos.location.lat},${datos.location.long})">Ver Mapa</button>
                </div>
            </div>
        </div>`;   
    }

    function seleccionarMapa(latitud,longitud){
        lat = latitud;
        lng = longitud;   
        initMap();   
    }

    function guardarHistorial(datos){
        let historial = [];
        if(JSON.parse(localStorage.getItem('historial'))){
            JSON.parse(localStorage.getItem('historial')).map((ciudad) => historial.push(ciudad) );
        }
        
        historial.push({
            'location':datos.location,
            'astronomy':datos.current_observation.astronomy,
            'condition':datos.current_observation.condition,
            'fechaconsulta':fecha()
        });

        localStorage.setItem('historial', JSON.stringify(historial));
    }

    function fecha(){
        let fecha = new Date();
        let ano = fecha.getFullYear();
        let mes = fecha.getMonth();
        let dia = fecha.getDate();
        let hora = fecha.getHours();
        let minuto = fecha.getMinutes();
        let segundo = fecha.getSeconds();

        let fechaYhora = `${ano}-${mes}-${dia} ${hora}:${minuto}:${segundo}`;

        return fechaYhora;
    }    

    </script>
</body>
</html>