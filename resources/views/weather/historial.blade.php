<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>APP - Clima Historial</title>

    <style>
        .card{
            border-radius: 10px;
        }
        .card img{
            border-radius: 10px;
        }
        #map {
            height: 80%;
        }        
    </style>
</head>
<body>

    <div class="container">
        <div class="row mt-3 mb-3">
            <h3>Historial de busqueda</h3>
            <div class="list-group" id="lista">
            </div>            
        </div>
    </div>

<script>
    
    let lista = document.getElementById('lista');
    verHistorial();

    function verHistorial(){
        
        historial = JSON.parse(localStorage.getItem('historial'));

        for (const ciudad of historial) {

            console.log(ciudad)

            lista.innerHTML += `
            <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">${ciudad.location.country} - ${ciudad.location.city} ${ciudad.location.region} <br> ${ciudad.location.timezone_id}</h5>
                <small>Fecha consulta: ${ciudad.fechaconsulta}</small>
                </div>
                <p class="mb-1">Temperatura ${ciudad.condition.temperature}</p>
                <p class="mb-1">Latitud ${ciudad.location.lat}</p>
                <p class="mb-1">Longitud ${ciudad.location.long}</p>
            </a>        
            `;         

        }
    }

</script>

</body>
</html>